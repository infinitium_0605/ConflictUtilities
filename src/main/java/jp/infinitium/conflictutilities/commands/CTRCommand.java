package jp.infinitium.conflictutilities.commands;

import com.github.ucchyocean.ct.ColorTeaming;
import com.github.ucchyocean.ct.ColorTeamingAPI;
import jp.infinitium.conflictutilities.ConflictUtilities;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class CTRCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        ColorTeamingAPI ctapi = ((ColorTeaming)ConflictUtilities.getColorTeaming()).getAPI();
        Economy economy = ConflictUtilities.getEconomy();
        if(sender instanceof ConsoleCommandSender){
            if(args.length == 3){
                if(args[0].equalsIgnoreCase("win")){
                    int amount = Integer.valueOf(args[1]);
                    Bukkit.broadcastMessage(ChatColor.YELLOW + "[CONFLICT] " + ChatColor.GREEN + "勝利チーム: " + args[2]);
                    for(Player player : ctapi.getTeamMembers(args[2])){
                        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
                        if(!economy.hasAccount(offlinePlayer)){
                            economy.createPlayerAccount(offlinePlayer);
                        }
                        economy.depositPlayer(offlinePlayer, amount);
                    }
                }else if(args[0].equalsIgnoreCase("lose")){
                    int amount = Integer.valueOf(args[1]);
                    for(Player player : ctapi.getTeamMembers(args[2])){
                        OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player.getUniqueId());
                        if(!economy.hasAccount(offlinePlayer)){
                            economy.createPlayerAccount(offlinePlayer);
                        }
                        economy.depositPlayer(offlinePlayer, amount);
                    }
                }else{
                    sender.sendMessage("エラー:引数を間違えています。");
                }
            }else{
                sender.sendMessage("エラー:引数を間違えています。");
            }
        }
        return true;
    }
}
