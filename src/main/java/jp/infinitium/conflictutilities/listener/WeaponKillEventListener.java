package jp.infinitium.conflictutilities.listener;

import com.hm.achievement.AdvancedAchievements;
import com.hm.achievement.listener.statistics.AbstractListener;
import jp.infinitium.conflictutilities.ConflictUtilities;
import me.DeeCaaD.CrackShotPlus.Events.WeaponKillEvent;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class WeaponKillEventListener implements Listener {
    @EventHandler
    public void onEvent(WeaponKillEvent event){
        Plugin plugin = ConflictUtilities.getInstance();
        String path = "user." + event.getKiller().getUniqueId() + "." + event.getWeaponTitle();
        int killAmount = plugin.getConfig().getInt(path);
        killAmount++;
        plugin.getConfig().set(path, killAmount);
        plugin.saveConfig();
        List<String> gunList = plugin.getConfig().getStringList("guns." + event.getWeaponTitle());
        for(String string : gunList){
            String[] strings = string.split(":");
            int amount = Integer.valueOf(strings[0]);
            if(killAmount == amount) {
                String achievement = strings[0];
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "aach give " + achievement + " " + event.getKiller().getDisplayName());
            }
        }
    }
}
