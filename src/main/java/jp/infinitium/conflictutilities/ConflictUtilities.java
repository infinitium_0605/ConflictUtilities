package jp.infinitium.conflictutilities;

import com.hm.achievement.AdvancedAchievements;
import com.hm.achievement.api.AdvancedAchievementsAPI;
import com.hm.achievement.api.AdvancedAchievementsAPIFetcher;
import jp.infinitium.conflictutilities.commands.CTRCommand;
import jp.infinitium.conflictutilities.commands.HatCommand;
import jp.infinitium.conflictutilities.listener.WeaponKillEventListener;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Optional;

public final class ConflictUtilities extends JavaPlugin {

    static Plugin instance;

    static Plugin colorTeaming;

    static Economy economy;

    @Override
    public void onEnable() {
        colorTeaming = getServer().getPluginManager().getPlugin("ColorTeaming");
        instance = this;
        saveDefaultConfig();
        RegisteredServiceProvider<Economy> economyProvider = getServer().getServicesManager().getRegistration(Economy.class);
        if (economyProvider != null) {
            this.economy = economyProvider.getProvider();
        }

        getCommand("ctr").setExecutor(new CTRCommand());
        getCommand("hat").setExecutor(new HatCommand());
        getServer().getPluginManager().registerEvents(new WeaponKillEventListener(), this);
    }

    public static Plugin getInstance() {
        return instance;
    }

    public static Economy getEconomy() {
        return economy;
    }

    public static Plugin getColorTeaming() {
        return colorTeaming;
    }

}
