package jp.infinitium.conflictutilities.commands;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class HatCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(sender instanceof Player){
            Player player = (Player)sender;
            PlayerInventory inventory = player.getInventory();
            if(inventory.getItemInMainHand().getType() == Material.AIR){
                player.sendMessage(ChatColor.YELLOW + "[CONFLICT] " + ChatColor.RED + "アイテムをもった状態で使用してください。");
            }else{
                ItemStack item = inventory.getItemInMainHand();
                ItemStack helmet = inventory.getHelmet();
                if(helmet == null){
                    inventory.setItemInMainHand(new ItemStack(Material.AIR));
                    inventory.setHelmet(item);
                }else{
                    inventory.setItemInMainHand(helmet);
                    inventory.setHelmet(item);
                }
                player.sendMessage(ChatColor.YELLOW + "[CONFLICT] " + ChatColor.GREEN + "アイテムを装着しました。");
            }
        }
        return true;
    }
}
